# FE 37 Step Project2 Forkio



## The following technologies have been used for the project:


```
HTML
Preprocessor SCSS
BEM
Adaptive design
Task manager GULP 
GRID and Flexbox
Vanila JS
NPM
VCS GitLab
Deployment ???? GitHub or GitLab ---------????? 

```

## The Team:

1. Roman Organ
2. Vlad Tulovskyi

## Team responsibilites distribution:

1. Development of task manager GULP files for the project - Roman Organ. 
2. Header and feedback sections - Roman Organ
3. Revolutionary editor, what you get and pricing - Vlad Tulovskiy.
4. Deployment - -----------???????

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
